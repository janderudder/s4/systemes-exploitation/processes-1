# Systèmes d'exploitation - TP1+TP2 : Les Processus


## Compiler et lancer un programme

	$ ./compile-run.sh src/programme.c

Les programmes sont placés dans le dossier local `bin`.


## Exercices

### Zombies

Source

	zombie1.c

Sortie du programme

	PARENT: pid =     9097,	pid_child =     9098,	ppid =     7091
	CHILD: pid =     9098,	pid_child =        0,	ppid =     9097

Commande `ps` (extrait)

	9097 pts/0    00:00:00 zombie
	9098 pts/0    00:00:00 zombie <defunct>

Conclusion:

Lorsqu'un enfant retourne avant son parent, il se retrouve en état "zombie" tant que le parent n'exécute pas un `wait` ou équivalent. Les enfants dans cet état ne peuvent pas être terminés. Il faut pour cela terminer le parent.
