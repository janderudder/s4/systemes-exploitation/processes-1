#include <stdio.h>
#include <stdlib.h>
#include <libgen.h> // basename
#include <unistd.h>
#include <sys/wait.h>



int child_process(const char* app_name)
{
    const pid_t pid = fork();

    if (pid == 0) {
        printf("(%s) <child 2> pid: %d, ppid: %d\n", app_name, getpid(), getppid());
        printf("\tDon't talk, think. That's a good rule of thumb for life. (Rick Grimes)\n");
        sleep(5);
        printf("(%s) <child 2> exiting...\n", app_name);
        return EXIT_SUCCESS;
    }
    else if (pid > 0) {
        printf("(%s) <child 1> pid: %d, ppid: %d, child-2-pid: %d\n", app_name, getpid(), getppid(), pid);
        printf("(%s) <child 1> exiting...\n");
        return EXIT_SUCCESS;
    }
    else {
        printf("(%s) <child X> fork error, code: %d\n", app_name, pid);
        return pid;
    }
}



int main(const int argc, char** argv)
{
    const char* app_name = basename(argv[0]);

    const int pid = fork();

    // parent process
    if (pid > 0) {
        printf("(%s) <parent> pid: %d, ppid: %d\n", app_name, getpid(), getppid());
        waitpid(pid, NULL, 0);
    }

    // child process
    else if (pid == 0)
    {
        child_process(app_name);
    }

    // fork error
    else {
        printf("(%s) fork error, code: %d\n", app_name, pid);
        return pid;
    }
}
