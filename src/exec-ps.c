#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main(const int argc, const char *const *const argv)
{
    if (argc < 2) {
        printf("Please pass number of function as argument.\n");
        printf("\t(1) execlp\n");
        printf("\t(2) execvp\n");
        printf("\t(3) execl\n");
        printf("\t(4) execv\n");
        return EXIT_SUCCESS;
    }

    switch (atoi(argv[1]))
    {
        case 1:
            execlp("ps", "ps", "aux", NULL);
        break;

        case 2: {
            char* const args[] = { "ps", "aux", NULL };
            execvp("ps",  args);
        }
        break;

        case 3:
            execl("/usr/bin/ps", "ps", "aux", NULL);
        break;

        case 4: {
            char* const args[] = { "ps", "aux", NULL };
            execv("/usr/bin/ps", args);
        }
        break;

        default:
            printf("invalid argument\n");
            return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
