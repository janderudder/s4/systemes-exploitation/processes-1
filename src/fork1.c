#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <signal.h>


int childProc()
{
    printf("CHILD\n---------------\n");
    printf("self PID: %d\n", getpid());
    printf("parent PID: %d\n", getppid());

    /**
     *  Either raise a signal or return a status code
     */
    raise(15);
    return 0x01000002; // which byte will be retained as status code ?
}



int parentProc(int childPid)
{
    printf("\nPARENT\n---------------\n");
    printf("self PID: %d\n", getpid());
    printf("parent PID: %d\n", getppid());
    printf("child PID: %d\n", childPid);
    return 0;
}



int main(int argc, char** argv)
{
    int childPid = fork();

    if (childPid == -1) {
        printf("Error, fork() returned %d\n", childPid);
    }
    else if (childPid != 0)
    {
        int childStatus;
        wait(&childStatus);

        parentProc(childPid);

        // information about child's process after its termination
        if (WIFEXITED(childStatus)) {
            printf("child's return status: %d\n", WEXITSTATUS(childStatus));
        }
        if (WIFSIGNALED(childStatus)) {
            printf("child's termination signal: %d\n", WTERMSIG(childStatus));
        }
        if (WIFSTOPPED(childStatus)) {
            printf("child's stop signal: %d\n", WSTOPSIG(childStatus));
        }
    }
    else {
        return childProc();
    }

    printf("\n");
    return 0;
}
