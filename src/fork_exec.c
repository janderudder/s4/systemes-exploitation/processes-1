#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>


int main(const int argc, const char *const *const argv)
{
    pid_t pid = fork();

    // parent process
    if (pid > 0) {
        int child_stat;
        wait(&child_stat);
        if (WIFEXITED(child_stat)) {
            printf("child's return value: %d\n", WEXITSTATUS(child_stat));
        }
    }

    // child process
    else if (pid == 0) {
        execlp("ps", "ps", "aux", NULL);
    }

    // error
    else {
        printf("fork error, code: %d\n", pid);
        return pid;
    }

    return EXIT_SUCCESS;
}
