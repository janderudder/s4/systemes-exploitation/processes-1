#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>


int main(int argc, char** argv)
{
    int n;

    if (argc < 2) {
        n = 1;
    }
    else {
        n = atoi(argv[1]);
    }

    printf("number of iterations: %d\n", n);

    for (int i=0; i<n; ++i)
    {
        int pid_f = fork();
        printf("pid = %8d, pid_fils = %8d, ppid = %8d, i=%8d\n", getpid(),
            pid_f, getppid(), i);
    }

    return EXIT_SUCCESS;
}
