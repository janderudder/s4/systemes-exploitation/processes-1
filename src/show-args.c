#include <stdio.h>
#include <libgen.h>


int main(int argc, char** argv)
{
    printf("basename(argv[0]): %s\n", basename(argv[0]));

    for (unsigned int i=0; i < argc; ++i) {
        printf("argv[%d]: %s\n", i, argv[i]);
    }

    return 0;
}
