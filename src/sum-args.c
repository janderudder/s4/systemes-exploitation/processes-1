#include <stdio.h>

int main(int argc, char** argv)
{
    int sum = 0;

    for (unsigned int i=1; i < argc; ++i) {
        int n;
        sscanf(argv[i], "%d", &n);
        sum += n;
    }

    printf("sum = %d\n", sum);

    return 0;
}
