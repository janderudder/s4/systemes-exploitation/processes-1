#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>


int main(int argc, char** argv)
{
    int pid = fork();

    // parent
    if (pid > 0) {
        printf("PARENT: pid = %8d,\tpid_child = %8d,\tppid = %8d\n", getpid(), pid, getppid());
        while (1) {
            sleep(10);
        }
    }
    // child
    else if (pid == 0) {
        printf("CHILD: pid = %8d,\tpid_child = %8d,\tppid = %8d\n", getpid(), pid, getppid());
    }
    // error
    else {
        printf("fork error\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
