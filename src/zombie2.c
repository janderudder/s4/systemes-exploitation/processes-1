#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>


int main(int argc, char** argv)
{
    int fork_value = fork();

    // parent process
    if (fork_value > 0) {
        printf("<parent> pid: %d, ppid: %d, child pid: %d\n",
            getpid(), getppid(), fork_value);
        return EXIT_SUCCESS;
    }

    // child process
    else if (fork_value == 0) {
        while (1) {
            printf("<child> pid: %d, ppid: %d\n", getpid(), getppid());
            sleep(1);
        }
    }

    // fork error
    else {
        printf("fork error, code: %d\n", fork_value);
        return fork_value;
    }

    return EXIT_SUCCESS;
}
